package de.inan.android.ai.stadtnamen

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("SearchActivity", "Start SearchActivity")
        setContentView(R.layout.activity_search)

        val search_button = findViewById<Button>(R.id.search_button)
        search_button.setOnClickListener {
            val intent = Intent(applicationContext, ResultActivity::class.java)
            intent.putExtra("Town", citySpinner.selectedItem.toString())
            intent.putExtra("District", districtSpinner.selectedItem.toString())
            intent.putExtra("Year", yearSpinner.selectedItem.toString())
            startActivity(intent)
        }

        val citySpinner = findViewById<Spinner>(R.id.citySpinner)
        val districtSpinner = findViewById<Spinner>(R.id.districtSpinner)
        val yearSpinner = findViewById<Spinner>(R.id.yearSpinner)
        val cityAdapter = ArrayAdapter<String>(applicationContext,
                android.R.layout.simple_spinner_item,
                resources.getStringArray(R.array.city_names))
        citySpinner.adapter = cityAdapter

        citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (!citySpinner.selectedItem.toString().equals("Wähle eine Stadt")) {
                    districtSpinner.isClickable = true
                    districtSpinner.visibility = View.VISIBLE
                    when (citySpinner.selectedItem.toString()) {
                        "Berlin" -> districtSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item, resources.getStringArray(R.array.district_names_berlin))
                        "Bonn" -> districtSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item,
                                resources.getStringArray(R.array.all_districts))
                        "Köln" -> districtSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item,
                                resources.getStringArray(R.array.all_districts))
                        "Düsseldorf" -> districtSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item,
                                resources.getStringArray(R.array.all_districts))
                        "Aachen" -> districtSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item,
                                resources.getStringArray(R.array.all_districts))
                    }
                } else {
                    districtSpinner.isClickable = false
                    districtSpinner.visibility = View.INVISIBLE
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }

        districtSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(!districtSpinner.selectedItem.toString().equals("Wähle einen Bezirk")) {
                    yearSpinner.isClickable = true
                    yearSpinner.visibility = View.VISIBLE
                    when (citySpinner.selectedItem.toString()) {
                        "Berlin" -> yearSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item, resources.getStringArray(R.array.year_berlin))
                        "Bonn" -> yearSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item, resources.getStringArray(R.array.year_bonn))
                        "Köln" -> yearSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item, resources.getStringArray(R.array.year_koeln))
                        "Düsseldorf" -> yearSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item, resources.getStringArray(R.array.year_duesseldorf))
                        "Aachen" -> yearSpinner.adapter = ArrayAdapter<String>(applicationContext,
                                android.R.layout.simple_spinner_item, resources.getStringArray(R.array.year_aachen))
                    }
                } else {
                    yearSpinner.isClickable = false
                    yearSpinner.visibility = View.INVISIBLE
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }

        yearSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
              if(!yearSpinner.selectedItem.toString().equals("Wähle ein Jahr")) {
                  search_button.isClickable = true
                  search_button.visibility = View.VISIBLE
              }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
    }
}

