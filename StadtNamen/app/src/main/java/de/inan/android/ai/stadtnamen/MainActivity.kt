package de.inan.android.ai.stadtnamen

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import org.jetbrains.anko.db.insert
import java.net.MalformedURLException
import java.net.URL
import kotlinx.coroutines.experimental.async

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("MainActivity", "Start Init")
        setContentView(R.layout.activity_main)

        val goButton = findViewById<Button>(R.id.goButton)

        goButton.setOnClickListener {
            val intent = Intent(applicationContext, SearchActivity::class.java)
            startActivity(intent)
        }

        async {
            // Berlin 2013 bis 2016
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/charlottenburg-wilmersdorf.csv", "Berlin", "Charlottenburg", 2016)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/friedrichshain-kreuzberg.csv", "Berlin", "Friedrichshain_Kreuzuberg", 2016)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2016/lichtenberg.csv", "Berlin", "Lichtenberg", 2016)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/marzahn-hellersdorf.csv", "Berlin", "Marzahn_Hellersdorf", 2016)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/mitte.csv", "Berlin", "Mitte", 2016)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/neukoelln.csv", "Berlin", "Neukoelln", 2016)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2016/pankow.csv", "Berlin", "Pankow", 2016)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/reinickendorf.csv", "Berlin", "Reinickendorf", 2016)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/spandau.csv", "Berlin", "Spandau", 2016)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2016/steglitz-zehlendorf.csv", "Berlin", "Steglitz_Zehlendorf", 2016)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/tempelhof-schoeneberg.csv", "Berlin", "Tempelhof_Schoeneberg", 2016)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2016/treptow-koepenick.csv", "Berlin", "Treptow_Koepenick", 2016)

            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/charlottenburg-wilmersdorf.csv", "Berlin", "Charlottenburg", 2015)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/friedrichshain-kreuzberg.csv", "Berlin", "Friedrichshain_Kreuzberg", 2015)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2015/lichtenberg.csv", "Berlin", "Lichtenberg", 2015 )
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/marzahn-hellersdorf.csv", "Berlin", "Marzahn_Hellersdorf", 2015)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/mitte.csv", "Berlin", "Mitte", 2015)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/neukoelln.csv", "Berlin", "Neukoelln", 2015)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2015/pankow.csv", "Berlin", "Pankow", 2015)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/reinickendorf.csv", "Berlin", "Reinickendorf", 2015)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/spandau.csv", "Berlin", "Spandau", 2015)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2015/steglitz-zehlendorf.csv", "Berlin", "Steglitz_Zehlendorf", 2015)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/tempelhof-schoeneberg.csv", "Berlin", "Tempelhof_Schoeneberg", 2015)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2015/treptow-koepenick.csv", "Berlin", "Treptow_Koepenick", 2015)

            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/charlottenburg-wilmersdorf.csv", "Berlin", "Charlottenburg", 2014)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/friedrichshain-kreuzberg.csv", "Berlin", "Friedrichshain_Kreuzberg", 2014)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2014/lichtenberg.csv", "Berlin", "Lichtenberg", 2014)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/marzahn-hellersdorf.csv", "Berlin", "Marzahn_Hellersdorf", 2014)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/mitte.csv", "Berlin", "Mitte", 2014)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/neukoelln.csv", "Berlin", "Neukoelln", 2014)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2014/pankow.csv", "Berlin", "Pankow", 2014)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/reinickendorf.csv", "Berlin", "Reinickendorf", 2014)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/spandau.csv", "Berlin", "Spandau", 2014)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2014/steglitz-zehlendorf.csv", "Berlin", "Steglitz_Zehlendorf", 2014)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/tempelhof-schoeneberg.csv", "Berlin", "Tempelhof_Schoeneberg", 2014)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2014/treptow-koepenick.csv", "Berlin", "Treptow_Koepenick", 2014)

            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/charlottenburg-wilmersdorf.csv", "Berlin", "Charlottenburg", 2013)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/friedrichshain-kreuzberg.csv", "Berlin", "Friedrichshain_Kreuzberg", 2013)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2013/lichtenberg.csv", "Berlin", "Lichtenberg", 2013)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/marzahn-hellersdorf.csv", "Berlin", "Marzahn_Hellersdorf", 2013)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/mitte.csv", "Berlin", "Mitte", 2013)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/neukoelln.csv", "Berlin", "Neukoelln", 2013)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2013/pankow.csv", "Berlin", "Pankow", 2013)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/reinickendorf.csv", "Berlin", "Reinickendorf", 2013)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/spandau.csv", "Berlin", "Spandau", 2013)
            buildDataPool(" http://www.berlin.de/daten/liste-der-vornamen-2013/steglitz-zehlendorf.csv", "Berlin", "Steglitz_Zehlendorf", 2013)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/tempelhof-schoeneberg.csv", "Berlin", "Tempelhof_Schoeneberg", 2013)
            buildDataPool("http://www.berlin.de/daten/liste-der-vornamen-2013/treptow-koepenick.csv", "Berlin", "Treptow_Koepenick", 2013)

            // Aachen 2013 bis 2015
            buildDataPool("http://offenedaten.aachen.de/dataset/aab1ab6e-c766-4d49-ac6d-7a225d348dd5/resource/fabcf517-b7ce-4dd7-b840-ae8403a6f4b2/download/pfb11400itmanagementopendataaachenvornamen2013.csv", "Aachen", "all",2013)
            buildDataPool("http://offenedaten.aachen.de/dataset/aab1ab6e-c766-4d49-ac6d-7a225d348dd5/resource/a2adf909-1f54-4dbb-ad68-2ce428ad0b3d/download/pfb11400itmanagementopendataaachenvornamen2014.csv", "Aachen", "all", 2014)
            buildDataPool("http://offenedaten.aachen.de/dataset/aab1ab6e-c766-4d49-ac6d-7a225d348dd5/resource/cc8e6798-ae36-465a-9a65-3dfe15b2246f/download/pfb11-400it-managementopendatadatenfb34aachenvornamen2015.csv", "Aachen", "all", 2015)

            // Köln 2014 bis 2017
            buildDataPool("https://offenedaten-koeln.de/sites/default/files/Vornamensstatistik_2014_0.csv", "Koeln", "all", 2014)
            buildDataPool("https://offenedaten-koeln.de/sites/default/files/Vornamen_Koeln_2015.csv", "Koeln", "all", 2015)
            buildDataPool("https://offenedaten-koeln.de/sites/default/files/Vornamen_Koeln_2016.csv", "Koeln", "all", 2016)
            buildDataPool("https://offenedaten-koeln.de/sites/default/files/Vornamen_Koeln_2017.csv", "Koeln", "all", 2017)

            // Bonn 2015 bis 2017
            buildDataPool("https://opendata.bonn.de/sites/default/files/Vornamensliste2015.csv", "Bonn", "all", 2015)
            buildDataPool("https://opendata.bonn.de/sites/default/files/vornamenbonn2016.csv", "Bonn", "all", 2016)
            buildDataPool("https://opendata.bonn.de/sites/default/files/VornamensstatistikBonn2017.csv", "Bonn", "all", 2017)

            // Düsseldorf 2017
            buildDataPool("https://opendata.duesseldorf.de/sites/default/files/Vornamenstatistik2017_Rangfolge.csv", "Duesseldorf", "all", 2017)
        }
    }

    suspend private fun buildDataPool(url: String, town: String, district: String, year: Int) {

        try {
            Log.i("MainActivity","Start Downloading " + url)
            val response = URL(url).readText()

            val responseString = response.lines().toMutableList()
            responseString.removeAt(0)

            var namesList: MutableList<String>
            var i = 0

            if(town=="Bonn") {
                while ( i < 60) {
                    namesList = responseString[i].split(";").toMutableList()
                    i ++
                    database.use {
                        insert("NameResult",
                                "town" to town,
                                "district" to district,
                                "year" to year,
                                "name" to namesList[1],
                                "amount" to namesList[0],
                                "gender" to namesList[2])
                    }
                }
            } else if (town=="Duesseldorf") {
                while (i < 60) {
                    namesList = responseString[i].split(";").toMutableList()
                    i++
                    database.use {
                        insert("NameResult",
                                "town" to town,
                                "district" to district,
                                "year" to year,
                                "name" to namesList[0],
                                "amount" to namesList[3],
                                "gender" to namesList[1])
                    }
                }
            }else {
                while (i < 60) {
                    namesList = responseString[i].split(";").toMutableList()
                    i++
                    database.use {
                        insert("NameResult",
                                "town" to town,
                                "district" to district,
                                "year" to year,
                                "name" to namesList[0],
                                "amount" to namesList[1],
                                "gender" to namesList[2])
                    }
                }
            }
            Log.i("MainActivity", "Insert into Database")
        } catch (e: MalformedURLException) {
            println("Not a valid URL")
        }
    }
}