package de.inan.android.ai.stadtnamen

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*


class MySQLiteHandler(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MySQLiteDB", null, 1) {
    companion object {
        private var instance: MySQLiteHandler? = null

        @Synchronized
        fun getInstance(ctx: Context): MySQLiteHandler {
            if (instance == null) {
                instance = MySQLiteHandler(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable("NameResult", true,
                "town" to TEXT,
                "district" to TEXT,
                "year" to INTEGER,
                "name" to TEXT,
                "amount" to INTEGER,
                "gender" to TEXT
                )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable("NameResult", true)
        onCreate(db)
    }
}

    val Context.database: MySQLiteHandler
        get() = MySQLiteHandler.getInstance(applicationContext)










