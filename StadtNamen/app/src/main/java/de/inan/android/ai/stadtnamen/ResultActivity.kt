package de.inan.android.ai.stadtnamen

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.select

class ResultActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("ResultActivity", "Start ResultActivity")
        setContentView(R.layout.activity_result)

        val selectedTown = intent.getStringExtra("Town")
        val selectedDistrict = intent.getStringExtra("District")
        val selectedYear = intent.getStringExtra("Year")

        Log.i("ResultActivity", " " + selectedTown + " " + selectedDistrict + " " + selectedYear)

        val girlButton = findViewById<Button>(R.id.button_girl)
        val boyButton = findViewById<Button>(R.id.button_boy)

        boyButton.setOnClickListener {
            fragmentManager.beginTransaction().replace(R.id.resultlist, BoyFragment()).commit()
        }

        girlButton.setOnClickListener {
            fragmentManager.beginTransaction().replace(R.id.resultlist, GirlFragment()).commit()
        }

        val nameParser = classParser<NameResult>()
        var resultList = database.use {
                            select("NameResult")
                                .whereSimple("(town = ?) and (district = ?) and (year = ?) and (gender = ?)",
                                            "Berlin", "Charlottenburg", 2015.toString(), "w")
                                .exec {
                                    parseList(nameParser)
                                }
                        }
        Log.i("ResultActivity", " " + resultList.toString())
        Log.i("ResultActivity", " " + resultList[1].toString())

        val resultGirl = findViewById<TextView>(R.id.result_girl)

    }


}

class NameResult (val town: String, val district: String, val year: Int, val name: String, val amount: String, val gender: String) {
    override fun toString(): String {
        return name + " " + amount + " " + gender
    }
}

